addEventListener("fetch", (event) => {
    event.respondWith(
        handleRequest(event).catch(
            (err) => new Response(err.stack, {
                status: 500
            })
        )
    )
})

async function handleRequest(event) {
    const {
        pathname
    } = new URL(event.request.url)

    const cacheUrl = new URL(event.request.url)

    // Example URL: /api/1990s-First-World-Problems/I%20feel%20bad%20for%20those/who%20don't%20use%20Jamstack

    // If an API request, increment the meme counter
    if (pathname.startsWith("/api")) {
        apiUrl = "http://apimeme.com/meme"
        meme = pathname.split("/")[2]
        topText = pathname.split("/")[3]
        bottomText = pathname.split("/")[4]

        // Try to get the KV key, if available
        const kvKey = meme + "/" + topText + "/" + bottomText
        const kvValue = await MEMES.get(kvKey)
        if (kvValue === null) {
            await MEMES.put(kvKey, 1,
                {
                    expirationTtl: 600
                }) // Expire in 10 minutes
        } else {
            // Increment the KV key value
            await MEMES.put(kvKey, parseInt(kvValue) + 1,
                {
                    expirationTtl: 600
                }) // Expire in 10 minutes
        }
    }

    // Try to use the Cloudflare cache, if available
    // https://developers.cloudflare.com/workers/examples/cache-api
    const cacheKey = new Request(cacheUrl.toString(), event.request)
    const cache = caches.default
    let response = await cache.match(cacheKey)

    // Fetch from the API origin if not cached in Cloudflare
    if (response) {
        return response
    } else if (pathname.startsWith("/api")) {
        let response = await fetch(apiUrl + "?meme=" + meme + "&top=" + topText + "&bottom=" + bottomText)
        response = new Response(response.body, response)

        // Cache for 30 seconds max in Cloudflare
        response.headers.append("Cache-Control", "s-maxage=30")
        event.waitUntil(cache.put(cacheKey, response.clone()))

        return response
    }

    return fetch("https://welcome.developers.workers.dev")
}