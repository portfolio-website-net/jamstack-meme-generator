addEventListener("fetch", (event) => {
    event.respondWith(
        handleRequest(event.request).catch(
            (err) => new Response(err.stack, {
                status: 500
            })
        )
    )
})

async function handleRequest(request) {
    const {
        pathname
    } = new URL(request.url)

    if (pathname.startsWith("/api")) {
        // Get the list of KV keys
        const list = await MEMES.list()
        let listValues = []

        // Add all the memes and count values to listValues
        for (const item in list.keys) {
            const value = await MEMES.get(list.keys[item].name)
            listValues.push({
                "name": list.keys[item].name,
                "count": value
            })
        }

        // Order by count descending
        listValues.sort(function(a, b) {
            return parseInt(a.count) >= parseInt(b.count) ? -1 : 1
        })

        // Get the top 10
        let topTenListValues = []
        for (var i = 0; i < listValues.length && i < 10; i++) {
            topTenListValues.push(listValues[i])
        }

        // Return the list of memes as JSON
        return new Response(JSON.stringify(topTenListValues), {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*", // Handle the Cross Origin Request Security (CORS) for the request
                "Access-Control-Expose-Headers": "cf-ray" // Allow the Cloudflare location header for parsing
            },
        })
    }

    return fetch("https://welcome.developers.workers.dev")
}