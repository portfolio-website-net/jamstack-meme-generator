var memeApiUrl = 'https://meme.jamstack-example.workers.dev/api/'
var memeLeaderboardApiUrl = 'https://meme-leaderboard.jamstack-example.workers.dev/api/'

var app = new Vue({
    el: '#app',
    data: {
        meme: null,
        topText: null,
        bottomText: null,
        memeOptions: [],
        memeExamples: [],
        locations: null,
        memeImageUrl: null,
        isMemeLoading: false,
        isMemeLoaded: false,
        isLoadingError: false,
        location: {
            city: null,
            country: null
        },
        memeLeaderboard: [],
        isMemeLeaderboardError: false
    },
    created() {
        this.initialize()
    },
    methods: {
        initialize: function () {
            if (this.memeOptions.length === 0) {
                this.initializeMemes()
            } else {
                this.initializeFromQuerystring()
            }
        },
        initializeMemes: function () {
            this.getJson('data/meme-options.json', function (data) {
                app.memeOptions = JSON.parse(data.target.response)

                app.getJson('data/meme-examples.json', function (data) {
                    app.memeExamples = JSON.parse(data.target.response)

                    app.getJson('data/cloudflare-locations.json', function (data) {
                        app.locations = JSON.parse(data.target.response)

                        app.initializeFromQuerystring()
                    })
                })
            })
        },
        getJson: function (path, onloadFunction) {
            let request = new XMLHttpRequest()
            request.onload = onloadFunction

            request.open('get', path, true)
            request.send()

            return request
        },
        initializeFromQuerystring: function () {
            let urlParams = new URLSearchParams(window.location.search)
            let meme = urlParams.has('meme') ? urlParams.get('meme') : null
            let topText = urlParams.has('topText') ? urlParams.get('topText') : null
            let bottomText = urlParams.has('bottomText') ? urlParams.get('bottomText') : null

            if (meme) {
                app.meme = meme
            } else {
                app.meme = app.memeExamples[0].meme
            }

            if (!this.meme) {
                app.meme = app.memeExamples[0].meme
            }

            if (topText) {
                app.topText = decodeURIComponent(topText)
            } else {
                app.topText = app.memeExamples[0].topText
            }

            if (bottomText) {
                app.bottomText = decodeURIComponent(bottomText)
            } else {
                app.bottomText = app.memeExamples[0].bottomText
            }

            if (meme && topText && bottomText) {
                app.generateMeme()
            }
        },
        generateMeme: function () {
            this.displayMeme()            
        },
        displayMeme: function () {
            let url = memeApiUrl + this.meme + '/' + encodeURIComponent(this.topText) + '/' + encodeURIComponent(this.bottomText)

            if (this.memeImageUrl !== url) {
                this.isMemeLoaded = false
                this.isMemeLoading = true
                this.isLoadingError = false

                this.memeImageUrl = url

                this.displayMemeLeaderboard()
            } else if (!this.isLoadingError) {
                this.memeImageLoad()
            }
        },
        memeImageLoad: function () {
            this.isMemeLoaded = true
            this.isMemeLoading = false
            this.isLoadingError = false
        },
        memeImageError: function () {
            this.isMemeLoaded = false
            this.isMemeLoading = false
            this.isLoadingError = true
        },
        displayMemeLeaderboard: function () {
            let request = app.getJson(memeLeaderboardApiUrl, function (data) {

                if (data.target.status === 200) {
                    let json = JSON.parse(data.target.response)

                    app.memeLeaderboard = []

                    for (var i = 0; i < json.length; i++) {
                        let meme = json[i].name.split('/')[0]
                        let topText = json[i].name.split('/')[1]
                        let bottomText = json[i].name.split('/')[2]
                        let count = json[i].count

                        if (typeof topText !== 'undefined' && typeof bottomText !== 'undefined') {
                            app.memeLeaderboard.push({
                                meme: meme,
                                topText: decodeURIComponent(topText),
                                bottomText: decodeURIComponent(bottomText),
                                count: count
                            })
                        }
                    }

                    let cfRay = request.getResponseHeader('cf-ray')
                    if (cfRay) {
                        let iataCode = cfRay.split('-')[1]
                        app.getLocation(iataCode)
                    } else {
                        app.location = {
                            city: null,
                            country: null
                        }
                    }
                }
                else {
                    app.isMemeLeaderboardError = true
                }
            })
        },
        getLocation: function (iataCode) {
            app.location = {
                city: null,
                country: null
            }

            let location = null
            for (i = 0; i < app.locations.length; i++) {
                if (app.locations[i].iataCode === iataCode) {
                    location = app.locations[i]
                    break
                }
            }

            if (location) {
                app.location = location
            }
        }
    }
})

appDiv = document.getElementById('app')
appDiv.style.display = 'block'